package buu.supawee.androidplusgame

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button

class MainGame : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main_game)

        val btnPlusGame = findViewById<Button>(R.id.btnPlusGame)
        btnPlusGame.setOnClickListener {
            val intent = Intent(MainGame@this,MainActivity::class.java)
            intent.putExtra("gamePlay","+")
            startActivity(intent)
        }

        val btnMinusGame = findViewById<Button>(R.id.btnMinusGame)
        btnMinusGame.setOnClickListener {
            val intent = Intent(MainGame@this,MainActivity::class.java)
            intent.putExtra("gamePlay","-")
            startActivity(intent)
        }

        val btnMultiGame = findViewById<Button>(R.id.btnMultiGame)
        btnMultiGame.setOnClickListener {
            val intent = Intent(MainGame@this,MainActivity::class.java)
            intent.putExtra("gamePlay","*")
            startActivity(intent)
        }
    }
}