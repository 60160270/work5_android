package buu.supawee.androidplusgame

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.TextView
import org.w3c.dom.Text

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        var won = 0
        var lose = 0
        val result = findViewById<Button>(R.id.ans1)
        val result2 = findViewById<Button>(R.id.ans2)
        val result3 = findViewById<Button>(R.id.ans3)
        val correctCount = findViewById<TextView>(R.id.txtCorrect)
        val incorrectCount = findViewById<TextView>(R.id.txtIncorrect)
        val txtResult = findViewById<TextView>(R.id.result)

        val intent = intent
        val gameType = intent.getStringExtra("gamePlay")
        val txtOperater = findViewById<TextView>(R.id.txtOperater)
        txtOperater.text = gameType

        var resultNumber = gameType?.let { gamestart(it) }

        result.setOnClickListener {
            if(result.text=="${resultNumber}"){
                won += 1
                correctCount.text = "${won}"
                txtResult.text = "Correct !"
            }else{
                lose += 1
                incorrectCount.text = "${lose}"
                txtResult.text = "Incorrect !"
            }
            resultNumber = gameType?.let { it1 -> gamestart(it1) }
        }
        result2.setOnClickListener {
            if(result2.text=="${resultNumber}"){
                won += 1
                correctCount.text = "${won}"
                txtResult.text = "Correct !"
            }else{
                lose += 1
                incorrectCount.text = "${lose}"
                txtResult.text = "Incorrect !"
            }
            resultNumber = gameType?.let { it1 -> gamestart(it1) }
        }
        result3.setOnClickListener {
            if(result3.text=="${resultNumber}"){
                won += 1
                correctCount.text = "${won}"
                txtResult.text = "Correct !"
            }else{
                lose += 1
                incorrectCount.text = "${lose}"
                txtResult.text = "Incorrect !"
            }
            resultNumber = gameType?.let { it1 -> gamestart(it1) }
        }

        val btnBack = findViewById<Button>(R.id.btnBack)
        btnBack.setOnClickListener {
            val intent = Intent(MainActivity@this,MainGame::class.java)
            startActivity(intent)
        }

    }
    fun gamestart(gameType : String):Int{
        val x = (0..10).random()
        val y = (0..10).random()
        val txtNumber1 = findViewById<TextView>(R.id.number1)
        txtNumber1.text = "${x}"
        val txtNumber2 = findViewById<TextView>(R.id.number2)
        txtNumber2.text = "${y}"
        if(gameType == "+"){
            randomChoice(x+y)
            return x+y
        }else if(gameType == "-"){
            randomChoice(x-y)
            return x-y
        }else if(gameType == "*"){
            randomChoice(x*y)
            return x*y
        }
        return 0
    }

    fun randomChoice(resultNumber : Int){
        val result = findViewById<Button>(R.id.ans1)
        val result2 = findViewById<Button>(R.id.ans2)
        val result3 = findViewById<Button>(R.id.ans3)
        val randomChoice = (1..3).random()
        if(randomChoice == 1){
            result.text = "${resultNumber}"

            result2.text = "${resultNumber+1}"

            result3.text = "${resultNumber+2}"

        }else if(randomChoice == 2){
            result.text = "${resultNumber-1}"

            result2.text = "${resultNumber}"

            result3.text = "${resultNumber+1}"
        }else{

            result.text = "${resultNumber-2}"

            result2.text = "${resultNumber-1}"

            result3.text = "${resultNumber}"
        }
    }
}